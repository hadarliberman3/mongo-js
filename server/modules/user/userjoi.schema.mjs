import Joi from "joi";


export const user_schema = Joi.object({

    first_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    last_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
  
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),

    phone: Joi.string().length(10).regex(/^\d+$/)
    
  })

  export function isValidUserSchema(req){
    const { error, value } = user_schema.validate( req.body );
      if(error) throw error;
      return value;
  }
  